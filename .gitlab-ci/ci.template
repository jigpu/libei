# vim: set expandtab shiftwidth=2 tabstop=8 textwidth=0 filetype=yaml:

{# You're looking at the template here, so you can ignore the below
   warning. This is the right file to edit #}
########################################
#                                      #
# THIS FILE IS GENERATED, DO NOT EDIT  #
#                                      #
########################################

.templates_sha: &template_sha 9f0eb526291fe74651fe1430cbd2397f4c0a819b # see https://docs.gitlab.com/ee/ci/yaml/#includefile

include:
  - project: 'freedesktop/ci-templates'
    ref: *template_sha
    file:
  {% for distro in distributions|sort(attribute="name") %}
  {% if not distro.does_not_have_ci_templates %}
      - '/templates/{{distro.name}}.yml'
  {% endif %}
  {% endfor %}
      - '/templates/ci-fairy.yml'

stages:
  - prep             # prep work like rebuilding the container images if there is a change
  - build            # for actually building and testing things in a container
  - distro           # distribs test
  - deploy

workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
    - if: $CI_PIPELINE_SOURCE == 'push'

variables:
  ###############################################################################
  # This is the list of packages required to build libei with the default       #
  # configuration.                                                              #
  #                                                                             #
  # Run dnf install/apt-get install/.. with the list of packages for your       #
  # distribution                                                                #
  #                                                                             #
  # See the documentation here:                                                 #
  # https://wayland.freedesktop.org/libinput/doc/latest/building_libinput.html  #
  ###############################################################################
{% for distro in distributions %}
  {{distro.name.upper()}}_PACKAGES: '{% for p in distro.packages %}{{p}} {% endfor %}'
  {{distro.name.upper()}}_PIP_PACKAGES: '{% for p in distro.pips %}{{p}} {% endfor %}'
{% endfor %}
  ############################ end of package lists #############################

  # these tags should be updated each time the list of packages is updated
  # changing these will force rebuilding the associated image
  # Note: these tags have no meaning and are not tied to a particular
  # libinput version
{% for distro in distributions %}
  {{"%-13s"| format(distro.name.upper() + '_TAG:')}}'{{distro.tag}}'
{% endfor %}

  FDO_UPSTREAM_REPO: libinput/libei

  MESON_BUILDDIR: "builddir"
  NINJA_ARGS: ''
  MESON_ARGS: ''
  MESON_TEST_ARGS: ''
  GIT_DEPTH: 1

.policy:
  retry:
    max: 2
    when:
      - runner_system_failure
      - stuck_or_timeout_failure
  # cancel run when a newer version is pushed to the branch
  interruptible: true

#################################################################
#                                                               #
#                          prep stage                           #
#                                                               #
#################################################################

fail-if-fork-is-not-public:
  stage: prep
  script:
    - |
      if [ $CI_PROJECT_VISIBILITY != "public" ]; then
           echo "*************************************************************************************"
           echo "Project visibility must be set to 'public'"
           echo "Change this in $CI_PROJECT_URL/edit under 'Visibility, project features, permissions'"
           echo "*************************************************************************************"
           exit 1
      fi
  except:
    - main@libinput/libei

# Re-generate the CI script and make sure it's the one currently checked in
# If this job fails, re-generate the gitlab-ci.yml script, see
# $SRCDIR/.gitlab-ci/generate-gitlab-ci.py
#
check-ci-script:
  extends:
    - .fdo.ci-fairy
  stage: prep
  script:
    - ci-fairy generate-template
    - git diff --exit-code && exit 0 || true
    - echo "Committed gitlab-ci.yml differs from generated gitlab-ci.yml. Please verify"
    - exit 1

#
# Verify that commit messages are as expected, signed-off, etc.
#

check-commit:
  extends:
    - .fdo.ci-fairy
  stage: prep
  script:
    - ci-fairy check-commits --junit-xml=results.xml
  except:
    - main@libinput/libei
  variables:
    GIT_DEPTH: 100
  artifacts:
    reports:
      junit: results.xml

#
# Verify that the merge request has the allow-collaboration checkbox ticked
#

check-merge-request:
  extends:
    - .fdo.ci-fairy
  stage: deploy
  script:
    - ci-fairy check-merge-request --require-allow-collaboration --junit-xml=results.xml
  artifacts:
    when: on_failure
    reports:
      junit: results.xml
  allow_failure: true

# Format anything python with python-black
#
python-black:
  extends:
    - .fdo.ci-fairy
  stage: prep
  before_script:
    - python3 -m venv venv
    - source venv/bin/activate
    - pip3 install black
  script:
    - black --check --diff . proto/ei-scanner

# Lint with Ruff
#
python-ruff:
  extends:
    - .fdo.ci-fairy
  stage: prep
  before_script:
    - python3 -m venv venv
    - source venv/bin/activate
    - pip3 install ruff
  script:
    - ruff check --ignore E741,E501 . proto/ei-scanner

#
# Build distribution-specific images used by the jobs in the build stage
#

{% for distro in distributions %}
{% for version in distro.versions %}
{{distro.name}}:{{version}}@container-prep:
  extends:
    - .fdo.container-build@{{distro.name}}
    - .policy
  stage: prep
  variables:
    GIT_STRATEGY: none
    FDO_DISTRIBUTION_VERSION: '{{version}}'
    FDO_DISTRIBUTION_PACKAGES: ${{distro.name.upper()}}_PACKAGES
    FDO_DISTRIBUTION_TAG: ${{distro.name.upper()}}_TAG
    FDO_DISTRIBUTION_EXEC: 'pip install ${{distro.name.upper()}}_PIP_PACKAGES'

{% endfor %}
{% endfor %}


#################################################################
#                                                               #
#                       build stage                             #
#                                                               #
#################################################################

.build@template:
  extends:
    - .policy
  stage: build
  script:
    - .gitlab-ci/meson-build.sh --run-test
  artifacts:
    name: "meson-logs-$CI_JOB_NAME"
    when: always
    expire_in: 1 week
    paths:
      - $MESON_BUILDDIR/meson-logs
    reports:
      junit: $MESON_BUILDDIR/meson-logs/.junit.xml
  dependencies: []

#
# Fedora
#

{% for distro in distributions if distro.use_for_custom_build_tests %}
{% set version = "{}".format(distro.versions|last()) %}
.{{distro.name}}-build@template:
  extends:
    - .fdo.distribution-image@fedora
    - .build@template
  variables:
    MESON_TEST_ARGS: '--no-suite=python'
    FDO_DISTRIBUTION_VERSION: '{{version}}'
    FDO_DISTRIBUTION_TAG: ${{distro.name.upper()}}_TAG
  needs:
    - "{{distro.name}}:{{version}}@container-prep"

default-build-release@{{distro.name}}:{{version}}:
  stage: distro
  extends:
    - .{{distro.name}}-build@template
  variables:
    MESON_ARGS: "-Dbuildtype=release"
    CFLAGS: "-Werror -Wno-error=vla-parameter"  # munit triggers -Wvla-parameter

build-no-libxkcommon-nodeps@{{distro.name}}:{{version}}:
  extends:
    - .{{distro.name}}-build@template
  before_script:
    - dnf remove -y libxkcommon-devel

build-no-doxygen@{{distro.name}}:{{version}}:
  extends:
    - .{{distro.name}}-build@template
  variables:
    MESON_ARGS: "-Ddocumentation=[]"
  before_script:
    - dnf remove -y doxygen hugo

valgrind@{{distro.name}}:{{version}}:
  extends:
    - .{{distro.name}}-build@template
  variables:
    MESON_TEST_ARGS: '--setup=valgrind'
  before_script:
    - dnf install -y valgrind

pytest@{{distro.name}}:{{version}}:
  extends:
    - .{{distro.name}}-build@template
  variables:
    MESON_TEST_ARGS: '--suite=python'

werror@{{distro.name}}:{{version}}:
  extends:
    - .{{distro.name}}-build@template
  variables:
    MESON_ARGS: '-Dwerror=true'
  allow_failure: true

abicheck@{{distro.name}}:{{version}}:
  extends:
    - .{{distro.name}}-build@template
  before_script:
    - git clone --depth=1 https://gitlab.freedesktop.org/hadess/check-abi
    - |
      pushd check-abi
      meson setup _build
      meson compile -C _build
      meson install -C _build
      popd
  script:
    - git remote add upstream$CI_JOB_ID https://gitlab.freedesktop.org/$FDO_UPSTREAM_REPO
    - git fetch --tags upstream$CI_JOB_ID
    - check-abi {{last_abi_break}} $CI_COMMIT_SHA
  only:
    - merge_requests
  variables:
    GIT_DEPTH: 50

event-type-check@{{distro.name}}:{{version}}:
  extends:
    - .{{distro.name}}-build@template
  script:
    - .gitlab-ci/meson-build.sh --skip-test
    - .gitlab-ci/check-event-values.py
  variables:
    PKG_CONFIG_PATH: $MESON_BUILDDIR/meson-uninstalled
  only:
    - merge_requests

minimum-meson@{{distro.name}}:{{version}}:
  extends:
    - .{{distro.name}}-build@template
  script:
    - pip uninstall -y meson
    - pip install "meson=={{minimum_meson_version}}"
    - .gitlab-ci/meson-build.sh --run-test

{% endfor %}

#################################################################
#                                                               #
#                        distro stage                           #
#                                                               #
#################################################################

{% for distro in distributions %}
{% for version in distro.versions %}
{{distro.name}}:{{version}}@default-build:
  stage: distro
  extends:
    - .build@template
    - .fdo.distribution-image@{{distro.name}}
  variables:
    FDO_DISTRIBUTION_VERSION: '{{version}}'
    FDO_DISTRIBUTION_TAG: ${{distro.name.upper()}}_TAG
    {# Where we have extra_variables defined, add them to the list #}
    {% if distro.build is defined and distro.build.extra_variables is defined %}
    {% for var in distro.build.extra_variables %}
    {{var}}
    {% endfor %}
    {% endif %}
  needs:
    - "{{distro.name}}:{{version}}@container-prep"

{% endfor %}
{% endfor %}

pages:
  stage: deploy
  extends:
    - .build@template
    - .fdo.distribution-image@{{pages.distro}}
  variables:
    FDO_DISTRIBUTION_VERSION: '{{pages.version}}'
    FDO_DISTRIBUTION_TAG: ${{pages.distro.upper()}}_TAG
    MESON_ARGS: "-Ddocumentation=protocol,api"
  script:
    - .gitlab-ci/meson-build.sh
    - rm -rf public/
    - mv "$MESON_BUILDDIR"/doc/protocol/ei/public/ public/
    - mv "$MESON_BUILDDIR"/doc/html/ public/api

  only:
    refs:
      - main
  artifacts:
    paths:
      - public
